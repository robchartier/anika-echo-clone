﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Anika.Common.Contracts;
using WolframAlpha.Api.v2;
using WolframAlpha.Api.v2.Requests;

namespace Anika.Wolfram
{
    public class WolfSkill : ISkillProvider
    {
        private readonly string _appId;

        public WolfSkill(string appId)
        {
            _appId = appId;
        }

        private List<string> _prefixList = new List<string>()
        {
            "look up ",
            "what ",
            "what's ",
            "who ",
            "who's ",
            "where ",
            "where's ",
            "when ",
            "when's ",
            "why ",
            "why's ",
            "how ",
            "how's ",
        };

        public async Task<string> Recongize(IRecognizedState state)
        {
            bool startsWith = false;
            var lower = state.Text;
            foreach (var p in _prefixList)
            {
                if (lower.StartsWith(p.ToLowerInvariant()))
                {
                    startsWith = true;
                    if (p == "look up ")
                    {
                        lower = lower.Substring(p.Length).Trim();
                    }
                    break;
                }
            }

            if (startsWith)
            {
                if (!string.IsNullOrEmpty(lower))
                {
                    if (Speak!=null) Speak("let me look that up for you");
                    var b = new QueryBuilder();
                    b.AppId = _appId;
                    b.Input = lower;
                    var r = new QueryRequest();
                    var result = await r.ExecuteAsync(b.QueryUri);
                    if (result.Success != "true") return "I'm not sure.";

                    if (result != null)
                    {
                        bool text = true;
                        var resultPod =
                            (from p in result.Pods where p.Id.ToLowerInvariant() == "result" select p).FirstOrDefault();

                        if (resultPod == null)
                        {
                            text = false;
                            resultPod =
                            (from p in result.Pods where p.Id.ToLowerInvariant() == "decimalapproximation" select p).FirstOrDefault();
                        }

                        if (resultPod != null)
                        {
                            var sub =
                                (from s in resultPod.SubPods where !string.IsNullOrEmpty(s.PlainText) select s.PlainText)
                                    .FirstOrDefault();

                            if (!text)
                            {
                                if (sub.EndsWith("...")) sub = sub.Replace("...", "");
                                double x = 0;
                                if (double.TryParse(sub, out x))
                                {
                                    sub = x.ToString("F");
                                }
                            }

                            if (!string.IsNullOrEmpty(sub)) return sub;
                        }
                    }
                }
            }
            return await Task.FromResult("");
        }

        public event Speak Speak;
    }
}
