﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Media.Capture;
using Windows.Storage;
using Anika.Common.Contracts;
using uPLibrary.Networking.M2Mqtt;

namespace MQTTSpeechInput
{
    public class MQTTSkill : ISkillProvider
    {
        private readonly string _brokerHost;
        private readonly int _brokerPort;
        private readonly bool _secure;
        private string[] _topics;
        private byte[] _qosLevels;
        private readonly string _clientId;
        private readonly string _username;
        private readonly string _password;

        public MQTTSkill(string brokerHost, int brokerPort, bool secure, string clientId = null, string username = null,
            string password = null)
        {
            _brokerHost = brokerHost;
            _brokerPort = brokerPort;
            _secure = secure;
            _clientId = clientId;
            _username = username;
            _password = password;

            if (_clientId == null) _clientId = "Anika-MQTT-Skill";
            _client = new MqttClient(_brokerHost, _brokerPort, _secure);
            _client.ConnectionClosed += _client_ConnectionClosed;
            _client.MqttMsgPublishReceived += _client_MqttMsgPublishReceived;
            _client.MqttMsgPublished += _client_MqttMsgPublished;
            _client.MqttMsgSubscribed += _client_MqttMsgSubscribed;
            _client.MqttMsgUnsubscribed += _client_MqttMsgUnsubscribed;

            if (string.IsNullOrEmpty(_username) && string.IsNullOrEmpty(_password))
            {
                _client.Connect(_clientId, _username, _password);
            }
            else
            {
                _client.Connect(_clientId);
            }
        }

        public void SubscribeForSpeechInput(string[] topics, byte[] qosLevels)
        {
            _topics = topics;
            _qosLevels = qosLevels;
            _client.Subscribe(_topics, _qosLevels);

        }



        private void _client_MqttMsgUnsubscribed(object sender,
            uPLibrary.Networking.M2Mqtt.Messages.MqttMsgUnsubscribedEventArgs e)
        {
            Debug.WriteLine(String.Format("_client_MqttMsgUnsubscribed:{0}", e.MessageId));
        }

        private void _client_MqttMsgSubscribed(object sender,
            uPLibrary.Networking.M2Mqtt.Messages.MqttMsgSubscribedEventArgs e)
        {
            Debug.WriteLine(String.Format("_client_MqttMsgSubscribed:{0} - {1}", e.MessageId, e.GrantedQoSLevels));

        }

        private void _client_MqttMsgPublished(object sender,
            uPLibrary.Networking.M2Mqtt.Messages.MqttMsgPublishedEventArgs e)
        {
            Debug.WriteLine(String.Format("_client_MqttMsgPublished:{0} - {1}", e.MessageId, e.IsPublished));

        }

        private void _client_MqttMsgPublishReceived(object sender,
            uPLibrary.Networking.M2Mqtt.Messages.MqttMsgPublishEventArgs e)
        {
            Debug.WriteLine(String.Format("_client_MqttMsgPublishReceived:{0} - {1} - {2} - {3} - {4}", e.DupFlag,
                e.Message, e.QosLevel, e.Retain, e.Topic));
            if (Speak != null) Speak(System.Text.Encoding.UTF8.GetString(e.Message));
        }

        private void _client_ConnectionClosed(object sender, EventArgs e)
        {
            Debug.WriteLine(String.Format("_client_ConnectionClosed:{0}", e));

            if (string.IsNullOrEmpty(_username) && string.IsNullOrEmpty(_password))
            {
                _client.Connect(_clientId, _username, _password);
            }
            else
            {
                _client.Connect(_clientId);

            }
        }

        private MqttClient _client;

        public async Task<string> Recongize(IRecognizedState state)
        {
            if (state == null || string.IsNullOrEmpty(state.Text) || state.NLPOutcomes == null) return await Task.FromResult("");
            string response = "";


            foreach (var outcome in state.NLPOutcomes)
            {
                if (outcome.Confidence > 0.50)
                {
                    if (outcome.Intent == "living_room_on")
                    {
                        response = "ok, i will turn the living room lights on";
                        break;
                    }
                    if (outcome.Intent == "living_room_off")
                    {
                        response = "ok, i will turn the living room lights off";
                        break;
                    }
                }
            }

            return await Task.FromResult(response);
        }

        public event Speak Speak;


    }
}