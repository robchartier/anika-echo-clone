﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Anika.Core.Speech;
using MQTTSpeechInput;
using Xunit;

namespace Anika.UnitTests
{
    public class MQTTTests
    {
        [Fact]
        [InlineData("turn on the living room lights")]
        [InlineData("turn the living room lights on")]
        public async Task TurnOnOffCommands(string command)
        {           
            var skill = new MQTTSkill(null, 0, false);
            var response = await skill.Recongize(new DefaultRecognizedState()
            {
                Text = command,
                Confidence = 1
            });

            Assert.NotNull(response);
            Assert.NotEmpty(response);

        }
    }
}
