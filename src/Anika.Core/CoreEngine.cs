﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using Anika.Core.Speech;
using Anika.Common.Contracts;

namespace Anika.Core
{
    public class CoreEngine
    {

        public static ITTSProvider DefaultTTSProvider = new Speech.DefaultTTSProvider();
        public static DefaultSpeechRecognitionProvider DefaultSpeechRecognitionProvider = new Speech.DefaultSpeechRecognitionProvider();

        private readonly ITTSProvider _ttsProvider;
        private readonly ISpeechRecognitionProvider _speechRecognitionProvider;
        private readonly IList<ISkillProvider> _skills;
        private readonly INLPProvider _nlpProvider;

        public string Trigger { get; set; }

        public CoreEngine(ITTSProvider ttsProvider, ISpeechRecognitionProvider speechRecognitionProvider, IList<ISkillProvider> skills, INLPProvider nlpProvider = null)
        {
            _ttsProvider = ttsProvider;
            _speechRecognitionProvider = speechRecognitionProvider;
            _skills = skills;
            _nlpProvider = nlpProvider;

            if (_skills != null)
            {
                foreach (var s in _skills)
                {
                    s.Speak += S_Speak;
                }
            }

        }

        private async void S_Speak(string text)
        {
            await _ttsProvider.Speak(text);
        }

        public async Task Speak(string text)
        {
            await _ttsProvider.Speak(text);
        }

        public async Task Initialize()
        {
            await _ttsProvider.Init();
            await _speechRecognitionProvider.Init();

            _speechRecognitionProvider.OnRecognizedState += _speechRecognitionProvider_OnRecognizedState;
        }

        private async void _speechRecognitionProvider_OnRecognizedState(IRecognizedState state)
        {
            if (!string.IsNullOrEmpty(Trigger))
            {
                if (!state.Text.ToLowerInvariant().StartsWith(Trigger.ToLowerInvariant())) return;
                state.Text = state.Text.Substring(state.Text.IndexOf(" ")).Trim();
            }
            if (string.IsNullOrEmpty(state.Text)) return;
            Debug.WriteLine(string.Format("Pumping speech through skills:{0}", state.Text));
            if (_skills != null)
            {
                if (_nlpProvider != null)
                {
                    try
                    {
                        state.NLPOutcomes = await _nlpProvider.Process(state);
                    }
                    catch (Exception)
                    {
                        //just ignore any nlp provider issues
                    }
                }
                foreach (var skill in _skills)
                {
                    try
                    {
                        var text = await skill.Recongize(state);
                        if (!string.IsNullOrEmpty(text))
                        {
                            await _ttsProvider.Speak(text);
                        }
                    }
                    catch (Exception e)
                    {
                        Debug.WriteLine(string.Format("Error during applying a skill:{0}, {1}", skill.GetType().FullName, e));                        
                    }
                }
            }
        }
    }
}
