﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using Windows.Media.SpeechRecognition;
using Anika.Common.Contracts;

namespace Anika.Core.Speech
{
    public class DefaultSpeechRecognitionProvider : ISpeechRecognitionProvider
    {

        private SpeechRecognizer recognizer;
        public event RecognizedState OnRecognizedState;

        public async Task Init()
        {
            recognizer = new SpeechRecognizer();

            recognizer.StateChanged += Recognizer_StateChanged;
            recognizer.ContinuousRecognitionSession.ResultGenerated += ContinuousRecognitionSession_ResultGenerated;
            recognizer.ContinuousRecognitionSession.Completed += ContinuousRecognitionSession_Completed;
            
            //recognizer.Timeouts.BabbleTimeout = TimeSpan.FromSeconds(5);
            recognizer.Timeouts.EndSilenceTimeout = TimeSpan.FromSeconds(2);
            recognizer.HypothesisGenerated += Recognizer_HypothesisGenerated;
            var contraint = new SpeechRecognitionTopicConstraint(SpeechRecognitionScenario.Dictation, "Anika");
            recognizer.Constraints.Add(contraint);
            SpeechRecognitionCompilationResult compilationResult = await recognizer.CompileConstraintsAsync();
            await recognizer.ContinuousRecognitionSession.StartAsync();


            Debug.WriteLine(string.Format("Status: {0}", compilationResult.Status));
        }

        private async void ContinuousRecognitionSession_Completed(SpeechContinuousRecognitionSession sender, SpeechContinuousRecognitionCompletedEventArgs args)
        {
            Debug.WriteLine(string.Format("ContinuousRecognitionSession_Completed: {0}", args.Status));

            await recognizer.ContinuousRecognitionSession.StartAsync();
            Debug.WriteLine("ContinuousRecognitionSession_Completed Current State:{0}", recognizer.State);

        }

        private void Recognizer_HypothesisGenerated(SpeechRecognizer sender, SpeechRecognitionHypothesisGeneratedEventArgs args)
        {
            Debug.WriteLine(string.Format("Recognizer_HypothesisGenerated: {0}", args.Hypothesis.Text));
            Debug.WriteLine("Recognizer_HypothesisGenerated Current State:{0}", recognizer.State);

        }

        private async void ContinuousRecognitionSession_ResultGenerated(SpeechContinuousRecognitionSession sender, SpeechContinuousRecognitionResultGeneratedEventArgs args)
        {
            
            Debug.WriteLine(string.Format("ContinuousRecognitionSession_ResultGenerated:Confidence:{0}, Text:{1}, Status:{2}", args.Result.RawConfidence, args.Result.Text, args.Result.Status));

            if (OnRecognizedState != null)
                OnRecognizedState(new DefaultRecognizedState()
                {
                    Confidence = args.Result.RawConfidence,
                    Text = args.Result.Text
                });

            Debug.WriteLine("ContinuousRecognitionSession_ResultGenerated Current State:{0}", recognizer.State);
            //await recognizer.ContinuousRecognitionSession.StartAsync();
        }

        private void Recognizer_StateChanged(SpeechRecognizer sender, SpeechRecognizerStateChangedEventArgs args)
        {

            Debug.WriteLine(string.Format("Recognizer_StateChanged:{0}", args.State));
            
        }
    }
}
