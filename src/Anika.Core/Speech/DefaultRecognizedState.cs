﻿using System.Collections.Generic;
using Anika.Common.Contracts;

namespace Anika.Core.Speech
{
    public class DefaultRecognizedState : IRecognizedState
    {
        public List<INLPOutcome> NLPOutcomes { get; set; }
        public double Confidence { get; set; }
        public string Text { get; set; }
    }
}
