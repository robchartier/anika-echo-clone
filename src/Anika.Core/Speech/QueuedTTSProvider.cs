﻿
using System;
using System.Collections.Concurrent;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Windows.Media.SpeechSynthesis;
using Windows.UI.Core;
using Windows.UI.Xaml.Controls;
using Anika.Common.Contracts;

namespace Anika.Core.Speech
{
    public class QueuedTTSProvider : ITTSProvider
    {

        private static BlockingCollection<string> _queue = new BlockingCollection<string>();

        SemaphoreSlim _semaphore = new SemaphoreSlim(1);

        public async Task Init()
        {
            mediaElement = new MediaElement();
            mediaElement.Volume = Double.MaxValue;
            var femaleVoice =
                (from s in SpeechSynthesizer.AllVoices where s.DisplayName == "Microsoft Zira Mobile" select s)
                    .FirstOrDefault();

            synth = new SpeechSynthesizer();
            synth.Voice = femaleVoice;

            Start();
        }

        private async Task Start()
        {
            Task.Factory.StartNew(async () =>
            {
                while (true)
                {
                    var text = "";
                    _queue.TryTake(out text);
                    if (!string.IsNullOrEmpty(text))
                    {

                        await
                            Windows.ApplicationModel.Core.CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(
                                CoreDispatcherPriority.Normal, () => { _Speak(text); });

                    }
                    await Task.Delay(50);
                }
            }, TaskCreationOptions.LongRunning);

        }

        public async Task Speak(string text)
        {
            _queue.Add(text);
        }

        private SpeechSynthesizer synth;
        private MediaElement mediaElement;

        private async Task _Speak(string text)
        {
            await _semaphore.WaitAsync();
            try
            {
                if (!string.IsNullOrEmpty(text))
                {
                    Debug.WriteLine(string.Format("Speaking:{0}", text));

                    SpeechSynthesisStream stream = await synth.SynthesizeTextToStreamAsync(text);

                    mediaElement.SetSource(stream, stream.ContentType);
                    mediaElement.Play();
                    mediaElement.Stop();

                }

            }
            catch (Exception e)
            {
                Debug.WriteLine(string.Format("ERROR:{0}" + e.ToString()));
            }
            finally
            {
                _semaphore.Release();
            }
        }

    }
}
