﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Windows.Media.SpeechSynthesis;
using Windows.UI.Core;
using Windows.UI.Xaml.Controls;
using Anika.Common.Contracts;

namespace Anika.Core.Speech
{
    public class DefaultTTSProvider : ITTSProvider
    {


        public async Task Init()
        {
            mediaElement = new MediaElement();

            var femaleVoice =
                (from s in SpeechSynthesizer.AllVoices where s.DisplayName == "Microsoft Zira Mobile" select s)
                    .FirstOrDefault();

            synth = new SpeechSynthesizer();
            synth.Voice = femaleVoice;


        }

        private SpeechSynthesizer synth;
        private MediaElement mediaElement;

        private async Task _speak(string text)
        {
            Debug.WriteLine(string.Format("Speaking:{0}", text));

            SpeechSynthesisStream stream = await synth.SynthesizeTextToStreamAsync(text);

            mediaElement.SetSource(stream, stream.ContentType);
            mediaElement.Play();

            mediaElement.Stop();

        }

        public async Task Speak(string text)
        {
            if (!string.IsNullOrEmpty(text))
            {
                await
                    Windows.ApplicationModel.Core.CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(
                        CoreDispatcherPriority.Normal, () => { _speak(text); });


            }
            //synth.Dispose();
        }
    }
}