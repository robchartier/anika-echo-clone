﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Anika.Common.Skills;
using Anika.Core;
using Anika.NLP.wit;
using Anika.Common.Contracts;
using Anika.Wolfram;
using MQTTSpeechInput;

namespace Anika
{
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();

            InitializeAnika();
        }

        private static Core.CoreEngine anika = null;

        public async Task InitializeAnika()
        {
            var ttsProvider = CoreEngine.DefaultTTSProvider;
            var recogProvider = CoreEngine.DefaultSpeechRecognitionProvider;

            var skills = new List<ISkillProvider>()
            {
                new TimeSkill()
            };


            var mqttInput = new MQTTSkill("test.mosquitto.org", 1883, false);
            mqttInput.SubscribeForSpeechInput(new[] {"rc/speech"}, new byte[] {0});
            skills.Add(mqttInput);

            var nlpProvider = new WitNLPProvider("NBG5RIE6LATVVZOABUTKBJJI6TZWABXG");


            var wolfy = new WolfSkill("VH4T8V-VTKPXLVT3W");
            skills.Add(wolfy);

            anika = new Core.CoreEngine(ttsProvider, recogProvider, skills, nlpProvider);

            anika.Trigger = "sofia";

            await anika.Initialize();

        }

        private async void ButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            await anika.Speak("hello there");
        }
    }
}