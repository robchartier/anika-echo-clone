﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Anika.Common.Contracts;

namespace Anika.Common.Skills
{
    public class TimeSkill : ISkillProvider
    {
        public async Task<string> Recongize(IRecognizedState state)
        {
            if (state.Confidence > 0.5 && state.Text.ToLowerInvariant()  == "what time is it")
            {
                Debug.WriteLine("*****TIME SKILL MATCHED!!******");
                return await Task.FromResult(string.Format("It is {0} {1}", System.DateTime.Now.ToString("D"), System.DateTime.Now.ToString("t")));
            }

            return null;
        }

        public event Speak Speak;
    }
}
