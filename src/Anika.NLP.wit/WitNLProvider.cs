﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Anika.Common.Contracts;

namespace Anika.NLP.wit
{
    public class WitNLPProvider : INLPProvider
    {
        private readonly string _authCode;

        private readonly HttpClient _client;
        public WitNLPProvider(string authCode)
        {
            _authCode = authCode;
            _client = new HttpClient();
            _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", authCode);
            
        }

        public async Task<List<INLPOutcome>> Process(IRecognizedState state)
        {
            return await Task.FromResult(await GetWit(state));
        }

        private async Task<List<INLPOutcome>> GetWit(IRecognizedState state)
        {
            if (state == null || string.IsNullOrEmpty(state.Text)) return await Task.FromResult<List<INLPOutcome>>(null);
            try
            {
                var json = await _client.GetStringAsync(string.Format("https://api.wit.ai/message?v={0}&q={1}", System.DateTime.Now.Ticks, state.Text));
                var response = Newtonsoft.Json.JsonConvert.DeserializeObject<WitResponse>(json);
                if (response != null && response.outcomes != null && response.outcomes.Length > 0)
                {
                    var lst = new List<INLPOutcome>();
                    foreach (var o in response.outcomes)
                    {
                        lst.Add(new WitNLPOutcome()
                        {
                            Confidence = o.confidence,
                            Intent = o.intent
                        });
                    }
                    return lst;
                }
            }
            catch (Exception e)
            {
            }
            return await Task.FromResult<List<INLPOutcome>>(null);
        }
    }
}
