﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Anika.NLP.wit
{

    public class WitResponse
    {
        public string msg_id { get; set; }
        public string _text { get; set; }
        public Outcome[] outcomes { get; set; }
    }

    public class Outcome
    {
        public string _text { get; set; }
        public string intent { get; set; }
        public Entities entities { get; set; }
        public float confidence { get; set; }
    }

    public class Entities
    {
    }

}
