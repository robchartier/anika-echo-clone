﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Anika.Common.Contracts;

namespace Anika.NLP.wit
{
    public class WitNLPOutcome : INLPOutcome
    {
        public string Intent { get; set; }
        public double Confidence { get; set; }
    }
}
