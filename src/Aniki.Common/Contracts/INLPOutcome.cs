﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Anika.Common.Contracts
{
    public interface INLPOutcome
    {
        string Intent { get; set; }
        double Confidence { get; set; }
    }
}
