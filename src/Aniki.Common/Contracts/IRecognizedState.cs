﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Anika.Common.Contracts
{

    public delegate void RecognizedState(IRecognizedState state);
    public interface IRecognizedState
    {

        List<INLPOutcome> NLPOutcomes { get; set; }
        double Confidence { get; set; }
        string Text { get; set; }
    }
}
