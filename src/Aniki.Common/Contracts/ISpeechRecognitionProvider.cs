﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Devices.Bluetooth.Advertisement;

namespace Anika.Common.Contracts
{
    public interface ISpeechRecognitionProvider
    {

        event RecognizedState OnRecognizedState;
        Task Init();
    }
}
