﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Anika.Common.Contracts
{

    public delegate void Speak(string text);
    public interface ISkillProvider
    {
        Task<string> Recongize(IRecognizedState state);

        event Speak Speak;
    }
}
