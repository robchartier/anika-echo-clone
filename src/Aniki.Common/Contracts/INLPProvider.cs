﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Anika.Common.Contracts
{
    public interface INLPProvider
    {
        Task<List<INLPOutcome>> Process(IRecognizedState state);
    }
}
